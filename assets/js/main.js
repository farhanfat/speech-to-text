var create_email = false;
var final_transcript = "";
var recognizing = false;
var ignore_onend;
var start_timestamp;
var iconnize = document.getElementById("iconnize");
var count = 0;
var langs = [["Bahasa Indonesia", ["id-ID"]]];
let flagFirst = false;
var recognition = new webkitSpeechRecognition();
let wd = window;
wd.startButton  
$(document).ready(function () {
  
  for (var i = 0; i < langs.length; i++) {
    select_language.options[i] = new Option(langs[i][0], i);
  }
  select_language.selectedIndex = 0;
  updateCountry();
  select_dialect.selectedIndex = 0;
  showInfo("info_start");

  function updateCountry() {
    for (var i = select_dialect.options.length - 1; i >= 0; i--) {
      select_dialect.remove(i);
    }
    var list = langs[select_language.selectedIndex];
    for (var i = 1; i < list.length; i++) {
      select_dialect.options.add(new Option(list[i][1], list[i][0]));
    }
    select_dialect.style.visibility =
      list[1].length == 1 ? "hidden" : "visible";
  }
  
  if (!("webkitSpeechRecognition" in window)) {
    upgrade();
  } else {
    recognition.continuous = true;
    recognition.interimResults = true;
    recognition.onstart = function () {
      recognizing = true;
      showInfo("info_speak_now");
      iconnize.className = "";
      iconnize.classList.add("fa", "fa-stop", "animate");
      enableDisable(true);
      flagFirst = true;
      enableDisableContent(flagFirst);
    };
    recognition.onerror = function (event) {
      if (event.error == "no-speech") {
        start_img.src = "https://speechtyping.com/images/mic.png";
        showInfo("info_no_speech");
        enableDisable(false);
        ignore_onend = true;
      }
      if (event.error == "audio-capture") {
        start_img.src = "https://speechtyping.com/images/mic.png";
        showInfo("info_no_microphone");
        enableDisable(false);
        ignore_onend = true;
      }
      if (event.error == "not-allowed") {
        if (event.timeStamp - start_timestamp < 100) {
          showInfo("info_blocked");
        } else {
          showInfo("info_denied");
        }
        ignore_onend = true;
      }
    };
    recognition.onend = function () {
      recognizing = false;
      console.log("after onend");
      if (ignore_onend) {
        return;
      }
      iconnize.className = "";
      iconnize.classList.add("fa", "fa-microphone");
      enableDisable(false);
      if (!final_transcript) {
        showInfo("info_start");
        return;
      }
      showInfo("info_start");
      if (create_email) {
        create_email = false;
        createEmail();
      }
    };
    recognition.onresult = function (event) {
      var interim_transcript = "";
      $(".gears-set").removeClass("fade").addClass("show");
      if (typeof event.results == "undefined") {
        recognition.onend = null;
        recognition.stop();
        upgrade();
        return;
      }
      for (var i = event.resultIndex; i < event.results.length; ++i) {
        if (event.results[i].isFinal) {
          final_transcript = event.results[i][0].transcript;
          addtext(final_transcript);
          $(".gears-set").removeClass("show").addClass("fade");
        } else {
          interim_transcript += event.results[i][0].transcript;
        }
      }
      final_span.innerHTML = linebreak(final_transcript);
      interim_span.innerHTML = linebreak(interim_transcript);
      if (final_transcript || interim_transcript) {
        showButtons("inline-block");
      }
    };
  }
});

function upgrade() {
    start_button.style.visibility = "hidden";
    alert("upgrade");
    showInfo("info_upgrade");
  }
  var two_line = /\n\n/g;
  var one_line = /\n/g;

  function linebreak(s) {
    return s.replace(two_line, " < p > < /p>").replace(one_line, " < br > ");
  }
  var first_char = /\S/;

  function capitalize(s) {
    return s.replace(first_char, function (m) {
      return m.toUpperCase();
    });
  }

  function copyButton() {
    if (recognizing) {
      recognizing = false;
      recognition.stop();
    }
    return fieldtoclipboard.copyfield(event, final_span);
  }

  function startButton(event) {

    if (recognizing) {
      recognition.stop();
      return;
    }
    recognition.lang = select_dialect.value;
    recognition.start();
    ignore_onend = false;
    interim_span.innerHTML = "";
    console.log(iconnize);
    iconnize.className = "";
    iconnize.classList.add("fa", "fa-microphone-slash");
    enableDisable(false);
    showInfo("info_allow");
    showButtons("none");
    start_timestamp = event.timeStamp;
  }

  function showInfo(s) {
    if (s) {
      for (var child = info.firstChild; child; child = child.nextSibling) {
        if (child.style) {
          child.style.display = child.id == s ? "inline" : "none";
        }
      }
      info.style.visibility = "visible";
    } else {
      info.style.visibility = "hidden";
    }
  }
  var current_style;

  function showButtons(style) {
    if (style == current_style) {
      return;
    }
    counter();
  }

  function myFunction() {
    if (recognizing) {
      recognizing = false;
      recognition.stop();
    }
    data = document.finaltextform.final_span.value;
    $("#data").val(data);
    $("#my-form").submit();
  }

  function myFunction6() {
    location.reload();
  }

  function counter() {
    var value = document.finaltextform.final_span.value;
    if (value.length == 0) {
      document.getElementById("wordCount").innerHTML = 0;
      document.getElementById("totalChars").innerHTML = 0;
      // document.getElementById('charCount').innerHTML = 0;
      document.getElementById("charCountNoSpace").innerHTML = 0;
      return;
    }
    var regex = /\s+/gi;
    var wordCount = value.trim().replace(regex, " ").split(" ").length;
    var totalChars = value.length;
    var charCount = value.trim().length;
    var charCountNoSpace = value.replace(regex, "").length;
    document.getElementById("charCountNoSpace").innerHTML = charCountNoSpace;
    document.getElementById("wordCount").innerHTML = wordCount;
    document.getElementById("totalChars").innerHTML = totalChars;
    // document.getElementById('charCount').innerHTML = charCount;
  }

  function printTextArea() {
    var printtext = document.finaltextform.final_span.value;
    childWindow = window.open(
      "",
      "childWindow",
      "location=yes, menubar=yes, toolbar=yes"
    );
    childWindow.document.open();
    childWindow.document.write(" < html > < head > < /head> < body > ");
    childWindow.document.write(printtext);
    childWindow.document.write("</body> < /html>");
    childWindow.print();
    childWindow.document.close();
    childWindow.close();
  }
  //Whatsapp
  function value_added() {
    if (recognizing) {
      recognizing = false;
      recognition.stop();
    }
    var send_content = final_transcript;
    if (send_content !== "") {
      var url_link = " Typed using www.speechtyping.com";
      var url = "https://web.whatsapp.com/send?text=" + send_content + url_link;
      document.getElementById("url_link").setAttribute("href", url);
    }
    //else{ alert("Please speaking first."); }
  }
  function enableDisable(param) {
    if (param) {
      $("#rip1").addClass("animate");
      $("#rip2").addClass("animate");
    } else {
      $("#rip1").removeClass("animate");
      $("#rip2").removeClass("animate");
    }
  }
  function enableDisableContent(param) {
    if (param) {
      $("#sectionContent")
        .addClass("show")
        .removeClass("fade")
        .removeAttr("hidden");
    } else {
      $("#sectionContent").addClass("fade").removeClass("show").attr("hidden");
    }
  }

  function saveTextAsFile() {
    var textToSave = document.getElementById("final_span").value;
    var textToSaveAsBlob = new Blob([textToSave], {
      type: "text/plain",
    });
    var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
    var fileNameToSaveAs = "speechtyping.txt";
    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "Download File";
    downloadLink.href = textToSaveAsURL;
    downloadLink.onclick = destroyClickedElement;
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }
  // for save as doc file
  function saveDocAsFile() {
    var textToSave = document.getElementById("final_span").value;
    var textToSaveAsBlob = new Blob([textToSave], {
      type: "text/Doc",
    });
    var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
    var fileNameToSaveAs = "speechtyping.doc";
    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "Download File";
    downloadLink.href = textToSaveAsURL;
    downloadLink.onclick = destroyClickedElement;
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }

  function destroyClickedElement(event) {
    document.body.removeChild(event.target);
  }
  function addtext(final_transcript) {
    final_transcript = final_transcript.replace(/ tanda koma/gi, ",");
    final_transcript = final_transcript.replace(/ tanda titik penuh/gi, ".");
    final_transcript = final_transcript.replace(/ tanda periode/gi, ".");
    final_transcript = final_transcript.replace(/ tanda usus besar/gi, ":");
    final_transcript = final_transcript.replace(/ tanda titik koma/gi, ";");
    final_transcript = final_transcript.replace(/ tanda tanya/gi, "?");
    final_transcript = final_transcript.replace(/ tanda seru/gi, "!");
    final_transcript = final_transcript.replace(/ tanda hubung/gi, " - ");
    final_transcript = final_transcript.replace(/ tanda hubung /gi, "-");
    final_transcript = final_transcript.replace(/ Mark ellipsis/gi, "...");
    final_transcript = final_transcript.replace(/ tanda apostrof/gi, "'");
    final_transcript = final_transcript.replace(
      / Tanda kutip tunggal terbuka /gi,
      " '"
    );
    final_transcript = final_transcript.replace(
      / Tanda kutip tunggal dekat/gi,
      "'"
    );
    final_transcript = final_transcript.replace(/ tanda kutip ganda; /gi, ' "');
    final_transcript = final_transcript.replace(
      / tanda kutip ganda dekat/gi,
      '"'
    );
    final_transcript = final_transcript.replace(
      / Tanda kurung terbuka/gi,
      " ["
    );
    final_transcript = final_transcript.replace(/ Tanda kurung tutup/gi, "]");
    final_transcript = final_transcript.replace(/ Tanda kurung buka/gi, " {");
    final_transcript = final_transcript.replace(
      / Tanda tutup kurung kurawal/gi,
      "}"
    );
    final_transcript = final_transcript.replace(/ Tanda kurung buka/gi, " (");
    final_transcript = final_transcript.replace(/ Tanda kurung tutup/gi, " )");
    document.finaltextform.final_span.value =
      document.finaltextform.final_span.value + " " + final_transcript;
    document.finaltextform.final_span.value =
      document.finaltextform.final_span.value.replace(/\s{2,}/g, " ");
  }
